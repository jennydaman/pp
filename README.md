# CINC 200: The Positivity Project

> Promoting a positive society through community engagement and reflection.

[Notes](https://docs.google.com/document/d/1SGav61Ii896GI5FxsCmX6o7jBpHGFR3-iGx7niJ40NI/edit?usp=sharing)

## Setup

Install dependencies

```bash
flutter packages get
```

To update the launcher icon,

```bash
flutter packages pub run flutter_launcher_icons:main -f <your config file name here>
```

To build the APK for release,

```bash
flutter build apk
```

https://pub.dartlang.org/packages/flutter_launcher_icons

## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).


