import 'package:flutter/material.dart';
import 'package:pp/diary/entry.dart';
import 'package:pp/diary/timeline.dart';
import 'package:pp/diary/entry_form.dart';
import 'dart:math';

List<Entry> initialEntries = <Entry>[
  Entry(
      date: new DateTime.now(),
      title: "Canada Goose Protest",
      message: "Joined a protest in front of the Canada Goose outlet "
        + "in opposition to their use of animal furs.",
      color: Colors.blueAccent),
  Entry(
      date: new DateTime.now().subtract(Duration(days: 2)),
      title: "Litter",
      message: "My friends and I spent an hour in Square Victoria "
        + "picking up stray trash and cigarette butts.",
      color: Colors.greenAccent),
  Entry(
    date: new DateTime.now().subtract(Duration(days: 3)),
    title: "Bought lunch for a homeless man",
    message: "at the Atwater metro station",
    color: Colors.yellowAccent,
  ),
  Entry(
    date: new DateTime.now().subtract(Duration(days: 4)),
    title: "Stood up for a stranger",
    message: "A cashier at IGA short changed the customer ahead of me " +
        "15 cents. He did not know French, so I helped by translating " +
        "the conversation.",
    color: Colors.orangeAccent,
  ),
  Entry(
      date: new DateTime.now().subtract(Duration(days: 31)),
      title: "Made an app to promote civic engagement",
      message:
          "I personally think it will be very useful.",
      color: Colors.redAccent)
];

// https://marcinszalek.pl/flutter/filter-menu-ui-challenge/?fbclid=IwAR3V3t6FibXr4nwbJPjBZ4Gm0E0-w2p1CHwCSeZOdB3N1NyA44x8p4uvZkA
class DiaryPage extends StatefulWidget {
  const DiaryPage({Key key}) : super(key: key);

  @override
  _DiaryState createState() => _DiaryState();
}

class _DiaryState extends State<DiaryPage> {
  final GlobalKey<AnimatedListState> _listKey =
      new GlobalKey<AnimatedListState>();
  ListModel _listModel;
  final Random r = new Random();
  int _colorIndex = COLOR_CYCLE.indexOf(initialEntries[0].color);

  void initState() {
    super.initState();
    _listModel = new ListModel(_listKey, initialEntries);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      /*
       * nested Scaffold in Scaffold (from main.dart)
       * https://github.com/flutter/flutter/issues/7036
       */
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Daily Diary'),
      ),
      body: TimelineWidget(
        listModel: _listModel,
        listKey: _listKey,
      ),
      floatingActionButton: new FloatingActionButton(
          tooltip: "Create Entry",
          child: Icon(Icons.create),
          onPressed: () async {
            String q = MOTIVATIONAL_QUOTES[r.nextInt(MOTIVATIONAL_QUOTES.length)];
            int nextColor = _colorIndex + 1 >= COLOR_CYCLE.length ? 0 : _colorIndex + 1;
            Entry newEntry = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => new EntryFormActivity(
                quote: q,
                color: COLOR_CYCLE[nextColor],
              )),
            );

            // newEntry would be null if back button was pressed
            if (newEntry != null) {
              setState(() {
                _listModel.insert(0, newEntry);
              });
              _colorIndex = nextColor;
            }
          }),
    );
  }
}

class TimelineWidget extends StatelessWidget {
  TimelineWidget({Key key, @required this.listModel, @required this.listKey})
      : super(key: key);
  final ListModel listModel;
  final GlobalKey<AnimatedListState> listKey;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        drawLine(),
        new AnimatedList(
          initialItemCount: listModel.items.length,
          key: listKey,
          itemBuilder: (context, index, animation) {
            return new DiaryRow(
              entry: listModel.items[index],
              animation: animation,
            );
          },
        ),
      ],
    );
  }
}

// TODO tap and hold for options (delete)
class DiaryRow extends StatelessWidget {
  final Entry entry;
  final Animation<double> animation;

  const DiaryRow({Key key, this.entry, this.animation}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const double _rightPadding = 16.0;
    return new FadeTransition(
      opacity: animation,
      child: new SizeTransition(
        sizeFactor: animation,
        child: new Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: new Row(
            children: <Widget>[
              new Dot(entry),
              new Expanded(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        Expanded(
                          child: new Text(
                            entry.title,
                            style: new TextStyle(fontSize: 18.0),
                          ),
                        ),
                        new Padding(
                          padding: const EdgeInsets.only(right: _rightPadding),
                          child: new Text(
                            entry.getDate(),
                            style: new TextStyle(
                                fontSize: 12.0,
                                color: Colors.grey,
                                fontStyle: FontStyle.italic),
                          ),
                        ),
                      ],
                    ),
                    new Padding(
                      padding: EdgeInsets.only(right: _rightPadding),
                      child: new Text(
                        entry.message,
                        style: new TextStyle(
                            fontSize: 14.0, color: Colors.grey[800]),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ListModel {
  ListModel(this.listKey, items) : this.items = new List.of(items);

  final GlobalKey<AnimatedListState> listKey;
  final List<Entry> items;

  AnimatedListState get _animatedList => listKey.currentState;

  void insert(int index, Entry item) {
    items.insert(index, item);
    _animatedList.insertItem(index, duration: new Duration(milliseconds: 150));
  }

  Entry removeAt(int index) {
    final Entry removedItem = items.removeAt(index);
    if (removedItem != null) {
      _animatedList.removeItem(
          index,
          (context, animation) => new DiaryRow(
                entry: removedItem,
                animation: animation,
              ),
          duration: new Duration(
              milliseconds: (150 + 200 * (index / length)).toInt()));
    }
    return removedItem;
  }

  int get length => items.length;

  Entry operator [](int index) => items[index];

  int indexOf(Entry item) => items.indexOf(item);
}
