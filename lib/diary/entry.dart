import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Entry {
  final DateTime date;
  String title;
  String message;
  Color color;

  Entry(
      {@required DateTime date,
      String title = "",
      String message = "",
      Color color = Colors.greenAccent})
      : date = date,
        title = title,
        message = message,
        color = color;

  /// formats the date as a short (probably fewer than 6 characters) String.
  String getDate() {
    var d = new DateTime.now().difference(date);
    if (d < Duration(hours: 24 * DateTime.daysPerWeek))
      return new DateFormat.E().format(date);
    return new DateFormat.MMMd().format(date);
  }
}
