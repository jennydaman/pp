import 'package:flutter/material.dart';
import 'entry.dart';

// https://cte.sfasu.edu/wp-content/uploads/2012/12/Service-Learning-Quotes.pdf
const MOTIVATIONAL_QUOTES = <String>[
  "Be the change you want to see.",
  "philanthropist (n) one who makes an active effort to promote human wellfare.",
  "Sometimes the most ordinary things could be made extraordinary, " +
      "simply by doing them with the right people.",
  "Make yourself present. Make them know who you are.",
  "Service learning is lit af fam.",
  "Diversity is being invited to the party; inclusion is being asked to dance.",
  "Why fit in when you were born to stand out?\n~Dr. Deuss",
  "We, together, are strong.",
  "Ask not what your country can do for you; " +
      "ask what you can do for your country.\n~John F. Kennedy",
  "Children are messages to a time we will never see.\n~Neil Postman",
  "No act of kindness, no matter how small, is ever wasted.",
  'After\nAll this time\nThe Sun never says to the Earth,\n"You owe me."' +
      "Look\nWhat happens\nWith a love like that," +
      "It lights the whole sky.\n~Daniel Ladinsky",
  "If opportunity doesn't knock, build a door.\n~Milton Berle",
  "It's easy to make a buck. It's tougher to make a difference.\n~Tom Brokaw",
  "The time is always right to do right.\n~Dr. Martin Luther King Jr.",
  "In doing we learn.\n~George Herbert",
  "A bit of fragrance always clings to the hand that gives the rose."
];

class EntryFormActivity extends StatelessWidget {
  final TextEditingController _titleController = new TextEditingController();
  final TextEditingController _messageController = new TextEditingController();
  final String _quote;
  final Color _color;

  EntryFormActivity({Key key, String quote = "", Color color = Colors.cyan})
      : _quote = quote,
        _color = color,
        super(key: key);

  String getTitle() {
    return _titleController.text;
  }

  String getMessage() {
    return _messageController.text;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        // https://docs.flutter.io/flutter/material/AppBar/leading.html
        title: Text('New Diary Entry'),
      ),
      body: Form(
        child: ListView(
          padding: EdgeInsets.all(8.0),
          children: <Widget>[
            TextFormField(
              decoration: InputDecoration(labelText: 'Title'),
              controller: _titleController,
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'What did you do?'),
              controller: _messageController,
              maxLines: 5,
            ),
            Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(top: 8.0, left: 4.0),
                      padding: EdgeInsets.only(left: 8.0, right: 16.0),
                      decoration: BoxDecoration(
                          border: Border(left: BorderSide(color: Colors.grey))),
                      child: Text(
                        _quote,
                        style: TextStyle(
                            color: Colors.grey, fontStyle: FontStyle.italic),
                      ),
                    ),
                  ),
                  FloatingActionButton(
                    onPressed: () {
                      Navigator.pop(
                          context,
                          new Entry(
                              date: DateTime.now(),
                              title: getTitle(),
                              message: getMessage(),
                              color: _color));
                    },
                    child: Icon(Icons.add),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
