import 'package:flutter/material.dart';
import 'entry.dart';

const double DOT_SIZE = 12.0;
const COLOR_CYCLE = <Color>[
  Colors.redAccent,
  Colors.orangeAccent,
  Colors.yellowAccent,
  Colors.greenAccent,
  Colors.blueAccent,
  Colors.purpleAccent
];

Widget drawLine() {
  return new Positioned(
    top: 0.0,
    bottom: 0.0,
    left: 32.0,
    child: new Container(
      width: 1.0,
      color: Colors.grey[300],
    ),
  );
}

class Dot extends StatefulWidget {
  final Entry entry;

  Dot(this.entry);

  @override
  _DotState createState() => _DotState();
}

class _DotState extends State<Dot> {
  // doesn't matter if it's -1
  int _colorIndex;

  @override
  void initState() {
    super.initState();
    _colorIndex = COLOR_CYCLE.indexOf(widget.entry.color);
  }

  void nextColor() {
    _colorIndex++;
    if (_colorIndex == COLOR_CYCLE.length) _colorIndex = 0;
    setState(() {
      widget.entry.color = COLOR_CYCLE[_colorIndex];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 32.0 - DOT_SIZE),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        child: new Padding(
          padding: new EdgeInsets.all(DOT_SIZE / 2),
          child: new Container(
            height: DOT_SIZE,
            width: DOT_SIZE,
            decoration: new BoxDecoration(
                shape: BoxShape.circle, color: widget.entry.color),
          ),
        ),
        onTap: nextColor,
      )
    );
  }
}
