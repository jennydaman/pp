import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

import 'diary.dart';
import 'feed.dart';
import 'organizations.dart';

void main() {
  debugPaintSizeEnabled = false;
  // TODO Richard's hard-coded "Organizations" activity page can't be rotated
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MyApp());
  });
  //runApp(const MyApp());
}

class MyApp extends StatefulWidget {

  const MyApp({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class ActivityPage {
  ActivityPage({
    String pageTitle,
    Widget pageWidget,
    IconData icon,
    String navTitle,
    Color color,
  })  : pageTitle = pageTitle,
        pageWidget = pageWidget,
        color = color,
        navItem = BottomNavigationBarItem(
          icon: Icon(icon),
          title: Text(navTitle),
          backgroundColor: color,
        );

  final String pageTitle;
  final Widget pageWidget;
  final Color color;
  final BottomNavigationBarItem navItem;
}

class _MyHomePageState extends State<MyApp> {
  int _currentIndex = 0;
  var _controller = PageController();
  List<ActivityPage> _pages;

  @override
  void initState() {
    super.initState();
    _pages = <ActivityPage>[
      ActivityPage(
          navTitle: 'Diary',
          pageTitle: 'Daily Diary',
          pageWidget: new DiaryPage(),
          icon: Icons.book,
          color: Colors.green),
      ActivityPage(
          navTitle: 'Feed',
          pageTitle: 'Local Feed',
          pageWidget: new FeedPage(),
          icon: Icons.rss_feed,
          color: Colors.blue),
      ActivityPage(
          navTitle: 'Organizations',
          pageTitle: 'Organizations',
          pageWidget: new OrganizationsPage(),
          icon: Icons.location_city,
          color: Colors.red),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Positivity Project',
      theme: ThemeData(
        // brightness: Brightness.dark, // dark theme
        primarySwatch: _pages[_currentIndex].color,
      ),
      home: new Scaffold(
        //appBar: AppBar(
          //title: Text(_pages[_currentIndex].pageTitle),
        //),
        body: PageView(
          controller: _controller,
          children: _pages
              .map<Widget>((ActivityPage navigationView) => navigationView.pageWidget)
              .toList(),
          onPageChanged: (int page) {
            setState(() {
              _currentIndex = page;
            });
          },
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: _pages
              .map<BottomNavigationBarItem>(
                  (ActivityPage navigationView) => navigationView.navItem)
              .toList(),
          currentIndex: _currentIndex,
          type: BottomNavigationBarType.fixed,
          onTap: (int index) {
            setState(() {
              _currentIndex = index;
              _controller.animateToPage(_currentIndex,
                  duration: Duration(milliseconds: 500), curve: Curves.easeOut);
            });
          },
        ),
      ),
    );
  }
}
