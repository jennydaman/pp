
import 'package:flutter/material.dart';
import 'package:pp/organizations/list.dart';
import 'package:pp/organizations/model.dart';

/*class OrganizationsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Available Organizations',
      theme: new ThemeData(
          brightness: Brightness.dark,
          backgroundColor: Colors.red,
      ),
      home: new MyHomePage(title: 'Organizations'),
    );
  }
}
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  var initialOrganizationgos = <Organization>[]
    ..add(new Organization('Alzheimer Society of Montreal', '4505 Notre-Dame Street West', 'The Alzheimer Society of Montreal is the leading non-profit health organization working nationwide to improve the quality of life for Canadians affected by Alzheimer\'s disease.'))
    ..add(new Organization('Chez Doris', '1430 Chomedey Street', 'Chez Doris is a charitable organization offering a daytime shelter 7 days a week helping women in difficult circumstance.'))
    ..add(new Organization('Contactivity Centre', '4695 De Maisonneuve West Boulevard', 'Contactivity Centre provides members 60 and over with a welcoming, caring, and safe place where a sense of community and social engagement is fostered.'))
    ..add(new Organization('MUHC Volunteer Services', '1650 Cedar Avenue', 'MUHC offers volunteers an opportunity to interact with adult patients, as well as with their families, it provides compassionate exemplary care with a specific commitment to help treat complex cases.'))
    ..add(new Organization('Native Montreal', '3183 Rue Saint-Jacques', 'An organization created by and for Aboriginal people, Native Montreal provides accessible services addressing community needs.'));

  Future<Null> _showNewOrganizationForm() async {
    Organization newOrganization = await Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (context) {
          return new AddOrganizationFormPage();
        },
      ),
    );
    if (newOrganization != null) {
      initialOrganizationgos.add(newOrganization);
    }
  }
  @override
  Widget build(BuildContext context) {
    var key = new GlobalKey<ScaffoldState>();
      return new Scaffold(
        key: key,
        appBar: new AppBar(
          backgroundColor: Colors.white,
          title: new Text(widget.title),

          actions: [
            new IconButton(
              icon: new Icon(Icons.add),
              onPressed: () => _showNewOrganizationForm(),
            ),
          ],
        ),
        body: new Container(
          decoration: new BoxDecoration(
            gradient: new LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              stops: [0.1, 0.5, 0.7, 0.9],
              colors: [
                Colors.white,
                Colors.white,
                Colors.white,
                Colors.white,
              ],
            ),
          ),
          child: new Center(
            child: new OrganizationList(initialOrganizationgos),
          ),
        ),
      );
  }
}*/
/*

class OrganizationsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'List of Organizations',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MainPage(),
    );
  }
}



class MainPage extends StatefulWidget {
  MainPage({Key key}) : super(key: key);

  @override
  _MainPageState createState() => new _MainPageState();
}

class _MainPageState extends State<MainPage> {
  double _imageHeight = 256.0;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Stack(
        children: <Widget>[
          _buildImage(),
          _buildTopHeader(),
        ],
      ),
    );
  }
  Widget _buildImage() {
    return new Positioned.fill(
      bottom:null,
      child: new ClipPath(
        clipper: new DiagonalClipper(),
        child: new Image.asset(
          'images/birds.jpg',
          fit: BoxFit.cover,
          height: _imageHeight,
          colorBlendMode: BlendMode.srcOver,
          color: new Color.fromARGB(120, 20, 10, 40),
        ),
      ),
    );
  }
}

Widget _buildTopHeader() {
  return new Padding(
    padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 32.0),
    child: new Row(
      children: <Widget>[
        //new Icon(Icons.menu, size: 32.0, color: Colors.white),
        new Expanded(
          child: new Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: new Text(
              "Organizations",
              style: new TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                  fontWeight: FontWeight.w300),
            ),
          ),
        ),
        //new Icon(Icons.linear_scale, color: Colors.white),
      ],
    ),
  );
}
*/

class OrganizationsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        brightness: Brightness.light,
        backgroundColor: Colors.red,
      ),
      home: new MainPage(),
    );
  }
}

class MainPage extends StatefulWidget {
  MainPage({Key key}) : super(key: key);

  @override
  _MainPageState createState() => new _MainPageState();
}

class _MainPageState extends State<MainPage> {
  double _imageHeight = 256.0;
  //double _imageHeight = window.screen.available.height;
  var initialOrganizationgos = <Organization>[]
    ..add(new Organization('Alzheimer Society of Montreal', '4505 Notre-Dame Street West', 'The Alzheimer Society of Montreal is the leading non-profit health organization working nationwide to improve the quality of life for Canadians affected by Alzheimer\'s disease.'))
    ..add(new Organization('Chez Doris', '1430 Chomedey Street', 'Chez Doris is a charitable organization offering a daytime shelter 7 days a week helping women in difficult circumstance.'))
    ..add(new Organization('Contactivity Centre', '4695 De Maisonneuve West Boulevard', 'Contactivity Centre provides members 60 and over with a welcoming, caring, and safe place where a sense of community and social engagement is fostered.'))
    ..add(new Organization('MUHC Volunteer Services', '1650 Cedar Avenue', 'MUHC offers volunteers an opportunity to interact with adult patients, as well as with their families, it provides compassionate exemplary care with a specific commitment to help treat complex cases.'))
    ..add(new Organization('Native Montreal', '3183 Rue Saint-Jacques', 'An organization created by and for Aboriginal people, Native Montreal provides accessible services addressing community needs.'));

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Stack(
        children: <Widget>[
          _buildImage(),
          //_buildTopHeader(),
          _buildList(),
          _buildBottomPart(),
        ],
      ),
    );
  }

  Widget _buildBottomPart() {
    return new Padding(
      padding: new EdgeInsets.only(top:_imageHeight-30),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildMyTasksHeader(),
          //_buildList(),
        ],
      ),
    );
  }

  Widget _buildMyTasksHeader() {
    var now = new DateTime.now();
    return new Padding(
      padding: new EdgeInsets.only(left: 32.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text(
            'Organizations',
            style: new TextStyle(fontSize: 34.0),
          ),
          new Text(
            //now.toString(),
            now.day.toString()+'/'+now.month.toString()+'/'+now.year.toString(),
            style: new TextStyle(color: Colors.grey, fontSize: 12.0),
          ),
        ],
      ),
    );
  }



  Widget _buildList(){
    return new Padding(
     // top:256.0,
      padding: new EdgeInsets.only(top:_imageHeight+23),
        child: new Container(
          //color: Color.fromRGBO(0, 0, 0, 100),
          child: new Center(

            child: new OrganizationList(initialOrganizationgos),
          ),
        ),
    );
  }
  Widget _buildImage() {
    return new Positioned.fill(
      bottom:null,
      child: new ClipPath(
        clipper: new DiagonalClipper(),
        child: new Image.asset(
         'images/change.png',
         fit: BoxFit.cover,
         height: _imageHeight,
         colorBlendMode: BlendMode.srcOver,
         color: new Color.fromARGB(120, 20, 10, 40),
        ),
      ),
    );
  }
}
class DiagonalClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.lineTo(0.0, size.height - 60.0);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
/*
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  Future<Null> _showNewOrganizationForm() async {
    Organization newOrganization = await Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (context) {
          return new AddOrganizationFormPage();
        },
      ),
    );
    if (newOrganization != null) {
      initialOrganizationgos.add(newOrganization);
    }
  }
  @override
  Widget build(BuildContext context) {
    var key = new GlobalKey<ScaffoldState>();
    return new Scaffold(
      key: key,
      body: new Container(

      ),
    );
  }
}
*/
