import 'package:flutter/material.dart';
import 'package:pp/organizations/card.dart';
import 'package:pp/organizations/model.dart';

class OrganizationList extends StatelessWidget {
  final List<Organization> organizationgos;

  OrganizationList(this.organizationgos);

  ListView _buildList(context) {
    return new ListView.builder(
      itemCount: organizationgos.length,
      itemBuilder: (context, int) {
        return new OrganizationCard(organizationgos[int]);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildList(context);
  }
}

