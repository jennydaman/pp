import 'package:flutter/material.dart';
import 'package:pp/organizations/model.dart';

class AddOrganizationFormPage extends StatefulWidget {
  @override
  _AddOrganizationFormPageState createState() => new _AddOrganizationFormPageState();
}

class _AddOrganizationFormPageState extends State<AddOrganizationFormPage> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController locationController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();

  void submitOrg(context) {
    if (nameController.text.isEmpty) {
      Scaffold.of(context).showSnackBar(
        new SnackBar(
          backgroundColor: Colors.redAccent,
          content: new Text('What is the organization\'s name?'),
        ),
      );
    } else {
      var newOrganization = new Organization(nameController.text, locationController.text,
          descriptionController.text);
      Navigator.of(context).pop(newOrganization);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Add an organization'),
        backgroundColor: Colors.black87,
      ),
      body: new Container(
        color: Colors.black54,
        child: new Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 8.0,
            horizontal: 32.0,
          ),
          child: new Column(
            children: [
              new Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: new Text('\nKnow about a good organization that isn\' on the list?\n\nLet us know about it!'),
              ),
              new Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: new TextField(
                    controller: nameController,
                    decoration: new InputDecoration(
                      labelText: 'Name',
                    )),
              ),
              new Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: new TextField(
                    controller: locationController,
                    decoration: new InputDecoration(
                      labelText: "Location",
                    )),
              ),
              new Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: new TextField(
                    controller: descriptionController,
                    decoration: new InputDecoration(
                      labelText: 'Description',
                    )),
              ),
              new Padding(
                padding: const EdgeInsets.all(16.0),
                child: new Builder(
                  builder: (context) {
                    return new RaisedButton(
                      color: Colors.indigoAccent,
                      child: new Text('Submit'),
                      onPressed: () => submitOrg(context),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
