import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pp/organizations/model.dart';

class OrganizationDetailPage extends StatefulWidget {
  final Organization organization;

  OrganizationDetailPage(this.organization);

  @override
  _OrganizationDetailPageState createState() => new _OrganizationDetailPageState();
}

class _OrganizationDetailPageState extends State<OrganizationDetailPage> {
  double organizationAvatarSize = 150.0;
  double _sliderValue = 10.0;

  Widget get organizationImage {
    return new Hero(
      tag: widget.organization,
      child: new Container(
        height: organizationAvatarSize,
        width: organizationAvatarSize,
        constraints: new BoxConstraints(),
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          //color: Colors.black,
          border: Border.all(
              color: Colors.black,
              width: .1
          ),
          boxShadow: [
            const BoxShadow(
                offset: const Offset(1.0, 2.0),
                blurRadius: 2.0,
                spreadRadius: -1.0,
                color: const Color(0x33000000)),
            const BoxShadow(
                offset: const Offset(2.0, 1.0),
                blurRadius: 3.0,
                spreadRadius: 0.0,
                color: const Color(0x24000000)),
            const BoxShadow(
                offset: const Offset(3.0, 1.0),
                blurRadius: 4.0,
                spreadRadius: 2.0,
                color: const Color(0x1F000000)),
          ],
          image: new DecorationImage(
            fit: BoxFit.cover,
            image: new NetworkImage(widget.organization.imageUrl ?? ''),
          ),
        ),
      ),
    );
  }

  Widget get organizationProfile {
    return new Container(
      padding: new EdgeInsets.symmetric(vertical: 32.0),
      decoration: new BoxDecoration(
        color: Colors.white,
      ),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          organizationImage,
          new Text(
            widget.organization.name,
            textAlign: TextAlign.center,
            style: new TextStyle(fontSize: 32.0),
          ),
          new Text(
            widget.organization.location,
            textAlign: TextAlign.center,
            style: new TextStyle(fontSize: 20.0),
          ),
          new Padding(
            padding:
            const EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
            child: new Text(widget.organization.description),
          ),
          rating
        ],
      ),
    );
  }

  Widget get rating {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Icon(
          Icons.star,
          size: 40.0,
          color: Colors.red,
        ),
        new Text(' ${widget.organization.rating} / 10',
            style: Theme.of(context).textTheme.display2),
      ],
    );
  }

  Widget get addYourRating {
    return new Column(
      children: <Widget>[
        new Container(
          padding: new EdgeInsets.symmetric(
            vertical: 16.0,
            horizontal: 16.0,
          ),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Flexible(
                flex: 1,
                child: new Slider(
                  activeColor: Colors.indigoAccent,
                  min: 0.0,
                  max: 10.0,
                  onChanged: (newRating) => updateSlider(newRating),
                  value: _sliderValue,
                ),
              ),
              new Container(
                width: 50.0,
                alignment: Alignment.center,
                child: new Text('${_sliderValue.toInt()}',
                    style: Theme.of(context).textTheme.display1),
              ),
            ],
          ),
        ),
        submitRatingButton,
      ],
    );
  }

  Widget get submitRatingButton {
    return new RaisedButton(
      onPressed: updateRating,
      child: new Text('Submit'),
      color: Colors.indigoAccent,
    );
  }

  void updateSlider(double newRating) {
    setState(() => _sliderValue = newRating);
  }

  void updateRating() {
    setState(() => widget.organization.rating = _sliderValue.toInt());
    _ratingDialog();
  }

  Future<Null> _ratingDialog() async {
    return showDialog(
      context: context,
      child: new SimpleDialog(
        children: <Widget>[
          TextField(
            autofocus: true,
            autocorrect: true,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Describe your experience! (Optional)'
            ),
          ),
          FlatButton(
            child: new Text('Confirm'),
            onPressed: (){Navigator.of(context, rootNavigator: true).pop();},
            //onPressed: () => Navigator.of(context).pop(),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      body: new ListView(
        children: <Widget>[organizationProfile, addYourRating],
      ),
    );
  }
}
