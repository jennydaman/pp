import 'package:flutter/material.dart';
import 'package:pp/organizations/detail_page.dart';
import 'package:pp/organizations/model.dart';

class OrganizationCard extends StatefulWidget {
  final Organization organization;

  OrganizationCard(this.organization);

  @override
  OrganizationCardState createState() {
    return new OrganizationCardState(organization);
  }
}

class OrganizationCardState extends State<OrganizationCard> {
  Organization organization;
  String renderUrl;

  OrganizationCardState(this.organization);

  void initState() {
    super.initState();
    renderOrganizationPic();
  }

  void renderOrganizationPic() async {
    await organization.getImageUrl();
    setState(() {
      renderUrl = organization.imageUrl;
    });
  }

  Widget get organizationImage {
    var organizationAvatar = new Hero(
      tag: organization,
      child: new Container(
        width: 100.0,
        height: 100.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          /*border: Border.all(
              color: Colors.black,
              width: .1
          ),*/

          image: new DecorationImage(
            fit: BoxFit.cover,
            image: new NetworkImage(renderUrl ?? ''),
          ),
        ),
      ),
    );

    var placeholder = new Container(
        width: 100.0,
        height: 100.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          gradient: new LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [Colors.black54, Colors.black, Colors.blueGrey[600]],
          ),
        ),
        alignment: Alignment.center,
        child: new Text(
          'ORG',
          textAlign: TextAlign.center,
        ));

    var crossFade = new AnimatedCrossFade(
      firstChild: placeholder,
      secondChild: organizationAvatar,
      crossFadeState: renderUrl == null
          ? CrossFadeState.showFirst
          : CrossFadeState.showSecond,
      duration: new Duration(milliseconds: 1000),
    );

    return crossFade;
  }

  Widget get organizationCard {
    return new Positioned(
      right: 0.0,
      child: new Container(
        width: 290.0,
        height: 115.0,
        child: new Card(
          color: Colors.white,
          child: new Padding(
            padding: const EdgeInsets.only(
              top: 8.0,
              bottom: 8.0,
              left: 64.0,
            ),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                new Text(widget.organization.name,
                    //style: Theme.of(context).textTheme.title,
                    style: TextStyle(color: Colors.black.withOpacity(1.0),fontSize: 20),
                ),
                new Text(widget.organization.location,
                    style: Theme.of(context).textTheme.subhead,),
                new Row(
                  children: <Widget>[
                    new Icon(
                      Icons.star,
                      color: Colors.red,
                    ),
                    new Text(': ${widget.organization.rating} / 10',
                        style: TextStyle(color: Colors.black.withOpacity(1.0)),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      onTap: () => showOrganizationDetailPage(),
      child: new Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: new Container(
          height: 115.0,
          child: new Stack(
            children: <Widget>[
              organizationCard,
              new Positioned(top: 7.5, child: organizationImage),
            ],
          ),
        ),
      ),
    );
  }

  showOrganizationDetailPage() {
    Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
      return new OrganizationDetailPage(organization);
    }));
  }
}
